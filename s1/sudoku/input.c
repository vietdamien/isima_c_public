/* *****************************************************************************
 * Project name: Sudoku
 * File name   : input
 * Author      : Damien GRID_SIZEguyen
 * Date        :
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "input.h"

char *getInput(char const *msg) {
    char *input, tmp[MAX_STR_LEN];
    
    printf(STR_FORMAT, msg);
    fgets(tmp, MAX_STR_LEN, stdin);
    if (tmp[strlen(tmp) - 1] == '\n') {
        tmp[strlen(tmp) -1] = '\0';
    }

    if ((input = malloc((strlen(tmp) + 1) * sizeof(char))) == NULL) {
        fprintf(stderr, "getInput: malloc problem.\n");
        exit(EXIT_FAILURE);
    }

    strcpy(input, tmp);
    return input;
}

int getInt(char const *msg) {
    char *line = getInput(msg);
    int input;

    if (sscanf(line, "%d%*c", &input) != 1) {
        fprintf(stderr, "getInt: sscanf problem.\n");
        free(line);
        exit(EXIT_FAILURE);
    }

    free(line);
    return input;
}

