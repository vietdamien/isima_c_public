/* *****************************************************************************
 * Project name: Sudoku
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Wednesday, September 25th 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "sudoku.h"

int main(void) {
    int grid[GRID_SIZE][GRID_SIZE];

    init(grid);
    display(grid);
    printf("=====\n");
    generate(grid);
    display(grid);

    printf("Removing value (l: 9; col: 9)\n");
    grid[8][8] = UNDEFINED;
    display(grid);

    printf("Cell %savailable.\n", input(grid) ? "" : "not ");
    display(grid);

    printf("Region 3-3 is %scorrect.\n", isValidRegion(grid, 3, 3) ? "" : "not ");
    printf("GRID is %scorrect.\n", isValidGrid(grid) ? "" : "not ");

    return EXIT_SUCCESS;
}

