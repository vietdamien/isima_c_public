/* *****************************************************************************
 * Project name: Sudoku
 * File name   : input
 * Author      : Damien Nguyen
 * Date        :
 * ****************************************************************************/

#ifndef __SUDOKU_INPUT_H__
#define __SUDOKU_INPUT_H__

#define MAX_STR_LEN 1024
#define STR_FORMAT  "%s"

char *getInput(char const *msg);

int getInt(char const *msg);

#endif /* __SUDOKU_INPUT_H__ */

