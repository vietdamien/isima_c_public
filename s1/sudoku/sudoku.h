/* *****************************************************************************
 * Project name: Sudoku
 * File name   : sudoku
 * Author      : Damien Nguyen
 * Date        : Wednesday, September 25th 2019
 * ****************************************************************************/

#ifndef __SUDOKU_SUDOKU_H__
#define __SUDOKU_SUDOKU_H__

#include <stdbool.h>

#define HORIZONTAL 0
#define VERTICAL   1

#define GRID_SIZE 9
#define UNDEFINED 0

bool input(int grid[][GRID_SIZE]);
bool isValidColumn(int grid[][GRID_SIZE], int column);
bool isValidGrid(int grid[][GRID_SIZE]);
bool isValidLine(int grid[][GRID_SIZE], int line);
bool isValidRegion(int grid[][GRID_SIZE], int line, int column);
bool isValidRow(int grid[][GRID_SIZE], int orientation, int index);

int generate(int grid[][GRID_SIZE]);
int init(int grid[][GRID_SIZE]);

void display(int grid[][GRID_SIZE]);

#endif /* __SUDOKU_SUDOKU_H__ */

