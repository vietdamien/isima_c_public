/* *****************************************************************************
 * Project name: Sudoku
 * File name   : sudoku
 * Author      : Damien Nguyen
 * Date        : Wednesday, September 25th 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#include "sudoku.h"

/**
 * Array of function pointers to choose whether a line or a column needs to be
 * checked.
 */
bool (*checkFuncPtr[])(int[][GRID_SIZE], int) = {
    isValidLine,
    isValidColumn
};

/**
 * Checks whether one cell among others in a line or column is defined and is
 * not a duplicate value.
 *
 * @param value    the value in the cell
 * @param neighbor the value of the neighbor
 * @return true if the cell is valid, false otherwise
 */
bool isValidCell(int value, int neighbor) {
    return value != neighbor && value != UNDEFINED && neighbor != UNDEFINED;
}

/**
 * Checks whether the input (line or column index, or cell value) is valid.
 *
 * @param value the input value
 * @return true if the input value is valid, false otherwise
 */
bool isValidInput(int value) {
    return value <= GRID_SIZE && value > UNDEFINED;
}

/**
 * Checks whether the given array is free of all duplicate values and has all
 * its cells defined.
 *
 * @param array the given array
 * @return true if it is, false otherwise
 */
bool isValidStrip(int grid[]) {
    bool isValid = true;
    int i, j;

    for (i = 0; i < GRID_SIZE; ++i) {
        for (j = i + 1; j < GRID_SIZE; ++j) {
            isValid &= isValidCell(grid[i], grid[j]);
        }
    }

    return isValid;
}

/**
 * Allows the end user to input a value at an input column and line in the
 * given sudoku grid.
 *
 * @param grid the given sudoku grid
 * @return true if every input is valid, false otherwise
 */
bool input(int grid[][GRID_SIZE]) {
    bool isAvailableCell;
    int line   = getInt("Line: ")   - 1;
    int column = getInt("Column: ") - 1;
    int value  = getInt("Value: ");

    if (!isValidInput(line) || !isValidInput(column) || !isValidInput(value)) {
        fprintf(stderr, "input: 0 < [line/column/value] <= %d.\n", GRID_SIZE);
        exit(EXIT_FAILURE);
    }

    if ((isAvailableCell = grid[line][column] == UNDEFINED)) {
        grid[line][column] = value;
    }

    return isAvailableCell;
}

/**
 * Checks whether the column that has the given index is valid.
 *
 * @param grid   the sudoku grid
 * @param column the given index
 * @return true if it is, false otherwise
 */
bool isValidColumn(int grid[][GRID_SIZE], int column) {
    int i, tmp[GRID_SIZE];

    for (i = 0; i < GRID_SIZE; ++i) {
        tmp[i] = grid[i][column - 1];
    }

    return isValidStrip(tmp);
}

/**
 * Checks whether the whole grid is valid.
 *
 * @param grid the sudoku grid
 * @return true if it is, false otherwise
 */
bool isValidGrid(int grid[][GRID_SIZE]) {
    bool isValid = true;
    int i, j;

    for (i = 1; i <= 3; ++i) {
        for (j = 1; j <= 3; ++j) {
            isValid &= isValidRegion(grid, i, j);
        }
    }
    for (i = 1; i <= GRID_SIZE; ++i) {
        for (j = 0; j < 2; ++j) {
            isValid &= isValidRow(grid, j, i);
        }
    }

    return isValid;
}

/**
 * Checks whether the line that has the given index is valid.
 *
 * @param grid the sudoku grid
 * @param line the given index
 * @return true if it is, false otherwise
 */
bool isValidLine(int grid[][GRID_SIZE], int line) {
    return isValidStrip(grid[line - 1]);
}

/**
 * Checks whether the region that has the given start line and the given start
 * column is valid.
 *
 * @param grid         the sudoku grid
 * @param regionLine   the given start line
 * @param regionColumn the given start column
 * @return true if it is, false otherwise
 */
bool isValidRegion(int grid[][GRID_SIZE], int regionLine, int regionColumn) {
    int i, j, k = 0, tmp[GRID_SIZE];

    regionLine   = (regionLine   - 1) * 3;
    regionColumn = (regionColumn - 1) * 3;
    for (i = regionLine; i < regionLine + 3; ++i) {
        for (j = regionColumn; j < regionColumn + 3; ++j) {
            tmp[k++] = grid[i][j];
        }
    }

    return isValidStrip(tmp);
}

/**
 * Checks whether the given row (vertical or horizontal) is valid.
 *
 * @param grid        the sudoku grid
 * @param orientation the orientation of the row to check
 * @param index       the index of the row to check
 * @return true if is, false otherwise
 */
bool isValidRow(int grid[][GRID_SIZE], int orientation, int index) {
    return (*checkFuncPtr[orientation])(grid, index);
}

int generate(int grid[][GRID_SIZE]) {
    int i, j;

    for (j = 0; j < GRID_SIZE; ++j) {
        for (i = 0; i < GRID_SIZE; ++i) {
            grid[j][i] = (i + j * 3 + j / 3) % GRID_SIZE + 1;            
        }
    }

    return GRID_SIZE * GRID_SIZE;
}

/**
 * Initializes the given sudoku grid to a 9 by 9 array of 0.
 *
 * @param the given sudoku grid
 * @return 0
 */
int init(int grid[][GRID_SIZE]) {
    int i, j;

    for (i = 0; i < GRID_SIZE; ++i) {
        for (j = 0; j < GRID_SIZE; ++j) {
            grid[i][j] = UNDEFINED;
        }
    }

    return 0;
}

/**
 * Displays the given sudoku grid.
 *
 * @param grid the given sudoku grid
 */
void display(int grid[][GRID_SIZE]) {
    int i, j;
    
    printf("\n");
    for (i = 0; i < GRID_SIZE; ++i) {
        for (j = 0; j < GRID_SIZE; ++j) {
            if (grid[i][j] != UNDEFINED) {
                printf("%2d%s", grid[i][j], ((j + 1) % 3) == 0 ? " " : "");
            } else {
                printf("  ");
            }
        }
        printf("\n%s", ((i + 1) % 3) == 0 ? "\n" : "");
    }
}

