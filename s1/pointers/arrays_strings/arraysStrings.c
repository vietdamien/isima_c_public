/* *****************************************************************************
 * Project name: Arrays and strings
 * File name   : arraysStrings
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 02 2019
 * ****************************************************************************/

#include "arraysStrings.h"

int count1(char *str) {
    int i = 0;

    while (*(str + i) != '\0') { ++i; }

    return i;
}

int count2(char *str) {
    char *tmp = str;

    while (*str != '\0') { ++str; }

    return str - tmp;
}

int count3(char *str) {
    char *tmp = str;

    /* The following line counts one more char (post incrementation).
     * while (*str++);
     */
    while (*(++str));

    return str - tmp;
}

