/* *****************************************************************************
 * Project name: Arrays and strings
 * File name   : arraysStrings
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 02 2019
 * ****************************************************************************/

#ifndef __ARRAYS_AND_STRINGS_ARRAYS_STRINGS_H__
#define __ARRAYS_AND_STRINGS_ARRAYS_STRINGS_H__

#define MAX_WORD_LEN 128

int count1(char *str);
int count2(char *str);
int count3(char *str);

#endif /* __ARRAYS_AND_STRINGS_ARRAYS_STRINGS_H__ */

