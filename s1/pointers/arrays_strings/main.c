/* *****************************************************************************
 * Project name: Arrays and strings
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 02 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "arraysStrings.h"

int main(void) {
    int tab[] = { 0, 1, 2, 3, 4, 5 };
    int  *p1;
    char *p2;
    char word[MAX_WORD_LEN] = "coucou";

    printf("===== ARRAYS/POINTERS =====\n%lu %lu %lu\n", sizeof(char), sizeof(int), sizeof(double));

    p1 = tab;
    ++p1;
    printf("*p1: %d\n", *p1);

    p2 = (char *) p1;
    p2 += sizeof(int);

    printf("*((int *)p2): %d\n", *((int *)p2));
    printf("*(p1 + 6):    %d\n", *(p1 + 6));

    p1 = NULL;
    /*printf("%d", *p1);*/

    printf("\n===== STRINGS =====\n");
    printf("count1(coucou): %d.\n", count1(word));
    printf("count2(coucou): %d.\n", count2(word));
    printf("count3(coucou): %d.\n", count3(word));

    return EXIT_SUCCESS;
}

