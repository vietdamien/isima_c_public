/* *****************************************************************************
 * Project name: Swap
 * File name   : swap
 * Author      : Damien Nguyen
 * Date        : Saturday, September 28 2019
 * ****************************************************************************/

#include "swap.h"

void swap(int *x, int *y) {
    int z = *x;
    *x = *y;
    *y = z;
}

