/* *****************************************************************************
 * Project name: Swap
 * File name   : swap
 * Author      : Damien Nguyen
 * Date        : Saturday, September 28 2019
 * ****************************************************************************/

#ifndef __SWAP_SWAP_H__
#define __SWAP_SWAP_H__

void swap(int *, int *);

#endif /* __SWAP_SWAP_H__ */

