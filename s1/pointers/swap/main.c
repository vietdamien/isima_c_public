/* *****************************************************************************
 * Project name: Swap
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Saturday, September 28 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "swap.h"

int main(void) {
    int x = 1, y = 2;

    printf("Before swapping, x = %d and y = %d.\n", x, y);
    swap(&x, &y);
    printf("After  swapping, x = %d and y = %d.\n", x, y);

    return EXIT_SUCCESS;
}

