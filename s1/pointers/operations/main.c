/* *****************************************************************************
 * Project name: Operations
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Saturday, September 28 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "operations.h"

int main(void) {
    int    i = 1, *ptri = &i;
    char   c1 = '1', *ptrc1 = &c1, c2 = '2', *ptrc2 = &c2;
    double d = 22, *ptrd = &d;

    printf("ptri = %p ptrc1 = %p \n", ptri,  ptrc1);
    printf("*ptri = %d et *ptrc1=%d\n",  *ptri, *ptrc1);
    /*ptri++;
    ptrc1++;*/

    printf("ptri = %p ptrc1 = %p \n",ptri, ptrc1);
    // cela permet de voir la taille d'un int et d'un char en memoire
    // sizeof(int)  sizeof(char)
    
    printf("ptrd = %p et *ptrd=%.f\n",  ptrd, *ptrd);
    ptrd += 2;
    printf("ptrd+2 = %p et *ptrd+2=%.f\n",  ptrd, *ptrd);

    printf("\n====================\n");
    printf("*ptrc1 = %c et *ptrc2 = %c\n", *ptrc1, *ptrc2);
    swap(ptrc1, ptrc2);
    printf("*ptrc1 = %c et *ptrc2 = %c\n", *ptrc1, *ptrc2);
    printf("c1 = %c et c2 = %c\n", c1, c2);

    return EXIT_SUCCESS;
}

