/* *****************************************************************************
 * Project name: Operations
 * File name   : operations
 * Author      : Damien Nguyen
 * Date        : Saturday, September 28 2019
 * ****************************************************************************/

#include "operations.h"

void swap(char *c1, char *c2) {
    char tmp = *c1;
    *c1 = *c2;
    *c2 = tmp;
}
