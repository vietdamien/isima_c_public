/* *****************************************************************************
 * Project name: Operations
 * File name   : operations
 * Author      : Damien Nguyen
 * Date        : Saturday, September 28 2019
 * ****************************************************************************/

#ifndef __OPERATIONS_OPERATIONS_H__
#define __OPERATIONS_OPERATIONS_H__

void swap(char *, char *);

#endif /* __OPERATIONS_OPERATIONS_H__ */

