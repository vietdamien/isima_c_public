/* *****************************************************************************
 * Project name: Revisions
 * File name   : revisions
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 01 2019
 * ****************************************************************************/

#ifndef __REVISIONS_REVISIONS_H__
#define __REVISIONS_REVISIONS_H__

#define MAX_WORD_LEN 128

#endif /* __REVISIONS_REVISIONS_H__ */

