/* *****************************************************************************
 * Project name: Revisions
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 01 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "revisions.h"

void read(char *dst) {
    printf("Str: ");
    scanf("%s", dst);
}

int main(void) {
    char *s;

    if ((s = malloc((MAX_WORD_LEN + 1) * sizeof(char))) == NULL) {
        perror("main: malloc.\n");
        exit(errno);
    }

    printf("What's your name: ");
    read(s);
    printf("Hi %s.\n", s);

    if (!strcmp(s, "zzz")) { printf("Weird.\n"); }

    free(s);
    return EXIT_SUCCESS;
}

