/* *****************************************************************************
 * Project name: Malloc
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 01 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "malloc.h"

int main(void) {
    double *array;
    int i;

    if ((array = malloc(ARRAY_SIZE * sizeof(double))) == NULL) {
        perror("main: malloc.\n");
        exit(errno);
    }

    for (i = 0; i < ARRAY_SIZE; ++i) {
        array[i] = i * i;
        printf("%9.f%s", array[i], ((i + 1) % 10 == 0) ? "\n" : "");
    }
    printf("\n");

    free(array);
    return EXIT_SUCCESS;
}

