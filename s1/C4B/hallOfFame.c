/* *****************************************************************************
 * Project name: Hall of fame
 * File name   : hallOfFame
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 08 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "hallOfFame.h"
#include "utilities.h"

void readData(FILE *in, data_t *data) {
    char str[STR_LEN];
    char name[DEFAULT_NAME_LEN], alias[DEFAULT_ALIAS_LEN];
    int tmp;

    readStr(in, name, DEFAULT_NAME_LEN);
    readStr(in, alias, DEFAULT_ALIAS_LEN);
    readStr(in, str, STR_LEN);

    if (!sscanf(str, "%d%*c", &tmp)) {
        fprintf(stderr, "readData: NaN\n");
        exit(EXIT_FAILURE);
    }

    strcpy(data->name, name);
    strcpy(data->alias, alias);
    data->score = tmp;
}

int arrayFromFilename(char *fileName, data_t array[]) {
    FILE *file;
    int size = 0;

    if ((file = fopen(fileName, "r")) == NULL) {
        perror("arrayFromFilename: fopen.\n");
        exit(errno);
    }

    do {
        data_t data = create();
        readData(file, &data);
        if (strcmp("", data.name) && strcmp("", data.alias)) {
            array[size++] = data;
        } else {
            freeFields(&data);
        }
    } while (!feof(file) && size < MAX_SIZE);

    fclose(file);
    return size;
}

void displayData(FILE *out, data_t data) {
    fprintf(out, "%s : %s avec %d\n", data.name, data.alias, data.score);
}

void inputData(FILE *in, data_t *data) {
    readData(in, data);
}

void freeFields(data_t *data) {
    free(data->name);
    free(data->alias);
}

data_t create(void) {
    data_t data;

    data.name  = (char *) malloc(DEFAULT_NAME_LEN  * sizeof(char));
    data.alias = (char *) malloc(DEFAULT_ALIAS_LEN * sizeof(char));

    if (data.name == NULL || data.alias == NULL) {
        perror("create: malloc.\n");
        exit(errno);
    }

    data.score = DEFAULT_SCORE;
    data.next  = NULL;

    return data;
}

list_t createList(void) { return NULL; }

void add(list_t *list, char *name, char *alias, int score) {
    if (isEmpty(*list) || strcmp(name, (*list)->name) < 0) {
        return addFirst(list, name, alias, score);
    }
    add(&((*list)->next), name, alias, score);
}

void addFirst(list_t *list, char *name, char *alias, int score) {
    data_t *cell;

    if ((cell = (data_t *) malloc(sizeof(data_t))) == NULL) {
        perror("addFirst: malloc.\n");
        exit(EXIT_FAILURE);
    }

    *cell = create();

    strcpy(cell->name, name);
    strcpy(cell->alias, alias);

    cell->score = score;
    cell->next  = *list;

    *list = cell;
}

void clear(list_t *list) {
    data_t *tmp;

    while (!isEmpty(*list)) {
        tmp = *list;
        *list = (*list)->next;
        freeFields(tmp);
        free(tmp);
    }
}

bool isEmpty(list_t list) { return list == NULL; }

int length(list_t list) {
    int len = 0;

    while (!isEmpty(list)) {
        ++len;
        list = list->next;
    }

    return len;
}

void display(list_t list) {
    while (!isEmpty(list)) {
        printf("Item\n");
        printf(
                "\t%s (%s)\n\t%d\n",
                list->name, list->alias, list->score
        );
        list = list->next;
    }
}

