/* *****************************************************************************
 * Project name: Hall of fame
 * File name   : hallOfFame
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 08 2019
 * ****************************************************************************/

#ifndef __HALL_OF_FAME_HALL_OF_FAME_H__
#define __HALL_OF_FAME_HALL_OF_FAME_H__

#include <stdbool.h>
#include <stdio.h>

#define DEFAULT_ALIAS_LEN 40
#define DEFAULT_NAME_LEN  100

#define DEFAULT_SCORE 0

#define MAX_SIZE 50

typedef struct data {
    char *name;
    char *alias;
    int   score;
    struct data *next;
} data_t, *list_t;

/* ===== C4A functions ===== */
int arrayFromFilename(char *fileName, data_t array[]);

void displayData(FILE *out, data_t data);
void inputData  (FILE *in, data_t *data);

void freeFields(data_t *data);

data_t create(void);

/* ===== List functions ===== */
list_t createList(void);
void add     (list_t *list, char *name, char *alias, int score);
void addFirst(list_t *list, char *name, char *alias, int score);
void clear   (list_t *list);

bool isEmpty(list_t list);

int length(list_t list);

void display(list_t list);

#endif /* __HALL_OF_FAME_HALL_OF_FAME_H__ */

