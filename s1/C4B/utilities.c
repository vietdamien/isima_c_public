/* *****************************************************************************
 * Project name: Hall of fame
 * File name   : utilities
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 08 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "utilities.h"

static void (*funcPtr[])(list_t *) = {
    NULL,
    show,
    insert,
    readFile,
    writeFile
};

void readStr(FILE *in, char *dst, size_t len) {
    int readLen;

    if (fgets(dst, len, in) && dst[(readLen = strlen(dst)) - 1] == '\n') {
        dst[readLen - 1] = '\0';
    }
}

char *readLine(char const *msg) {
    char *line, tmp[STR_LEN];
    int len;

    printf("%s", msg);
    if (fgets(tmp, STR_LEN, stdin) && tmp[(len = strlen(tmp)) - 1] == '\n') {
        tmp[len - 1] = '\0';
    }

    if ((line = (char *) malloc((len * sizeof(char)))) == NULL) {
        perror("readLine: malloc.\n");
        exit(errno);
    }

    strcpy(line, tmp);
    return line;
}

int readInt(char const *msg) {
    char *tmp = readLine(msg);
    int i;

    if (sscanf(tmp, "%d%*c", &i) != 1) {
        fprintf(stderr, "readInt: NaN.\n");
        free(tmp);
        exit(EXIT_FAILURE);
    }

    free(tmp);
    return i;
}

bool handle(int choice, list_t *list) {
    bool handled;

    if ((handled = (*funcPtr[choice]) != NULL)) {
        (*funcPtr[choice])(list);
    }

    return handled;
}

void insert(list_t *list) {
    char *name, *alias;

    add(
            list,
            (name = readLine("Name: ")),
            (alias = readLine("Alias: ")),
            readInt("Score: ")
    );

    free(name);
    free(alias);
}

void insertReadData(FILE *in, list_t *list) {
    char name[DEFAULT_NAME_LEN], alias[DEFAULT_ALIAS_LEN], tmp[STR_LEN];
    int score;

    readStr(in, name, DEFAULT_NAME_LEN);
    readStr(in, alias, DEFAULT_ALIAS_LEN);
    readStr(in, tmp, STR_LEN);
    score = sscanf(tmp, "%d%*c", &score) == 1
                ? score
                : DEFAULT_SCORE;

    add(list, name, alias, score);
}

void readFile(list_t *list) {
    char tmpSize[STR_LEN];
    FILE *in;
    int i, size;

    if ((in = fopen(LIST_PATH, "r")) == NULL) {
        perror("readFile: fopen.\n");
        exit(errno);
    }

    readStr(in, tmpSize, STR_LEN);
    if (sscanf(tmpSize, "%d%*c", &size) != 1) {
        fprintf(stderr, "readFile: NaN.\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < size; ++i) {
        insertReadData(in, list);
    }

    fclose(in);
}

void show(list_t *list) {
    printf("\n");
    display(*list);
    printf("\n");
}

void writeFile(list_t *list) {
    data_t *tmp = *list;
    FILE *out;
    int len = length(tmp);

    if ((out = fopen(LIST_PATH, "w")) == NULL) {
        perror("writeFile: fopen.\n");
        exit(errno);
    }

    fprintf(out, "%d\n", len);
    while (!isEmpty(tmp)) {
        fprintf(out, "%s\n%s\n%d\n", tmp->name, tmp->alias, tmp->score);
        tmp = tmp->next;
    }
    printf("Saved!\n");

    fclose(out);
}

int menu(void) {
    printf("===== MENU =====\n");
    printf("\t1 - Display\n");
    printf("\t2 - Insert\n");
    printf("\t3 - Load\n");
    printf("\t4 - Save\n");
    printf("\t* - Quit\n");

    return readInt("Choice: ");
}

