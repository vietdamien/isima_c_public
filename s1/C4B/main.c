/* *****************************************************************************
 * Project name: Hall of fame
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 08 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hallOfFame.h"
#include "utilities.h"

int main(void) {
    bool handled;
    int choice = 0;
    list_t list = createList();

    do { handled = handle((choice = menu()), &list); } while (handled);

    clear(&list);
    return EXIT_SUCCESS;
}

