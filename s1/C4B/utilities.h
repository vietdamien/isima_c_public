/* *****************************************************************************
 * Project name: Hall of fame
 * File name   : utilities
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 08 2019
 * ****************************************************************************/

#ifndef __HALL_OF_FAME_UTILITIES_H__
#define __HALL_OF_FAME_UTILITIES_H__

#include <stdbool.h>
#include <stdio.h>

#include "hallOfFame.h"

#define LIST_PATH "list.txt"

#define STR_LEN 512

/* ===== Core functions ===== */
char *readLine(char const *msg);
int   readInt (char const *msg);
void  readStr(FILE *in, char *dst, size_t len);

/* ===== User functions =====*/
bool handle(int choice, list_t *);

void insert   (list_t *);
void readFile (list_t *);
void show     (list_t *);
void writeFile(list_t *);

int menu(void);

#endif /* __HALL_OF_FAME_UTILITIES_H__ */

