/* *****************************************************************************
 * Project name: C3
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 3rd 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "matrix.h"
#include "teZZt.h"
#include "vector.h"

BEGIN_TEST_GROUP(vector)

TEST(Example) {
	int b = 3;
	
	CHECK (3 == b);
	//CHECK (0 == b);
} 

TEST(A) {
   char  buffer[1024];
   float v1[] = { 1.0, 2.0, 3.0 };
   FILE *file = fmemopen(buffer, 1024, "w");
   // REQUIRE ( NULL != file);

   writeVector(file, v1, 3);

   fclose(file);

   CHECK(0 == strcmp(buffer, "[ 1.000 2.000 3.000 ]"));
}

TEST(B) {
   char   buffer[1024];
   float *v1 = NULL;

   v1 = (float *) malloc(3 * sizeof(float));
   REQUIRE(NULL != v1) ; // allocation impossible ?

   v1[0] = 3.0;
   v1[1] = 2.0;
   v1[2] = 1.0;

   FILE *file = fmemopen(buffer, 1024, "w");
   REQUIRE(NULL != file); // ouverture du flux impossible ?

   writeVector(file, v1, 3);

   fclose(file);

   CHECK(0 == strcmp(buffer, "[ 3.000 2.000 1.000 ]"));

   freeVector(&v1);
}

TEST(ReadA, "Checking vectors' order") {
   int    order;
   float *vec = NULL;

   vec = readVector("v1.txt", &order);
   CHECK(3 == order);
   freeVector(&vec);

   vec = readVector("v2.txt", &order);
   CHECK(6 == order);
   freeVector(&vec);
   
   vec = readVector("v3.txt", &order);
   CHECK(3 == order);
   freeVector(&vec);

   // fichier non existant
   vec = readVector("v0.txt", &order);
   CHECK(NULL == vec);
   freeVector(&vec);
}

TEST(ReadB, "Checking v1.txt") {
   int    order;
   float *vec;

   vec = readVector("v1.txt", &order);
   CHECK  (3    == order);
   REQUIRE(NULL != vec  ); // si vec est null, on ne veut pas continuer le test

   CHECK(EQ(vec[0], 5.0));
   CHECK(EQ(vec[1], 6.0));   // :-))
   CHECK(EQ(vec[2], 7.0)); 

   freeVector(&vec);
}

TEST(ReadC, "checking v2.txt") {
   int    order;
   float *vec;

   vec = readVector("v2.txt", &order);
   CHECK  (6    == order);
   REQUIRE(NULL != vec  ); // si vec est null, on ne veut pas continuer le test

   CHECK(EQ(vec[0], 6.1));
   CHECK(EQ(vec[1], 5.2));
   CHECK(EQ(vec[2], 4.3));
   CHECK(EQ(vec[3], 3.4));
   CHECK(EQ(vec[4], 2.5));
   CHECK(EQ(vec[5], 1.6));

   freeVector(&vec);
}

TEST(ReadD, "checking v3.txt") {
   int    order;
   float *vec;

   vec = readVector("v3.txt", &order);
   CHECK  (3    == order );
   REQUIRE(NULL == vec   ); // v3 est incomplet, on veut que ce soit nul

   freeVector(&vec);
}

TEST(PVA) {
	float *v1    = NULL;
	float  v2[3] = { 1.0, 2.0, 3.0 };
	int    order = 0;
	
	v1 = readVector("v1.txt", &order);
	CHECK  (3    == order);
	REQUIRE(NULL != v1   );

	CHECK(EQ(38, scalarProduct(v1, v2, order)));

    freeVector(&v1);
}

TEST(UserInput, "User input") {
    float *v1, *v2;
    int i, order;

    printf("Input an order for both vectors: ");
    scanf("%d%*c", &order);

    v1 = malloc(order * sizeof(float));
    v2 = malloc(order * sizeof(float));
    CHECK(NULL != v1);
    CHECK(NULL != v2);
    if (v1 == NULL || v2 == NULL) {
        perror("TEST: UserInput, malloc.\n");
        exit(errno);
    }

    printf("Vector #1\n");
    for(i = 0; i < order; ++i) {
        printf("\tx%d = ", i + 1);
        scanf("%f%*c", &v1[i]);
    }

    printf("Vector #2\n");
    for(i = 0; i < order; ++i) {
        printf("\tx%d = ", i + 1);
        scanf("%f%*c", &v2[i]);
    }

    printf("<v1 | v2> = %.3f\n", scalarProduct(v1, v2, order));

    free(v1);
    free(v2);
}

TEST(ReadMatrix, "checking m1.txt") {
    char buffer[1024];
    FILE *file;
    float **m = NULL;
    int order = 0;

    m = readMatrix("m1.txt", &order);
    CHECK  (3    == order);
    REQUIRE(NULL != m    );

    if ((file = fmemopen(buffer, 1024, "w")) == NULL) {
        perror("TEST: ReadMatrix, fmemopen.\n");
        exit(errno);
    }

    writeMatrix(file, m, order);
    fclose(file);

    CHECK(0 == strcmp(buffer, "[\n1.000 2.000 3.000 \n2.000 4.000 5.000 \n3.000 7.000 1.000 \n]"));

    freeMatrix(&m, order);
}

TEST(MatrixProduct) {
    float **m = NULL, *v = NULL, *result = NULL, answer[3] = { 38, 69, 64 };
    int i, order = 0;

    m = readMatrix("m1.txt", &order);
    v = readVector("v1.txt", &order);

    result = product(m, v, order);

    printf("Product\n[ ");
    for (i = 0; i < order; ++i) {
        printf("%.3f ", result[i]);
    }
    printf("]\n");

    for (i = 0; i < order; ++i) {
        CHECK(result[i] == answer[i]);
    }

    freeMatrix(&m, order);
    freeVector(&v);
    freeVector(&result);
}

END_TEST_GROUP(vector)

int main(void) {
	RUN_TEST_GROUP(vector);

 	return EXIT_SUCCESS;;
}
