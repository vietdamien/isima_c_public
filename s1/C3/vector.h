/* *****************************************************************************
 * Project name: C3
 * File name   : vector
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 3rd 2019
 * ****************************************************************************/

#ifndef __C3_VECTOR_H__
#define __C3_VECTOR_H__

#include <stdio.h>

#define MAX_WORD_LEN 32

float *readVector(char *fileName, int *order);
void   writeVector(FILE *out, float *vector, int order);

float scalarProduct(float *v1, float *v2, int order);

void freeVector(float **vector);

#endif /* __C3_VECTOR_H__ */

