/* *****************************************************************************
 * Project name: C3
 * File name   : matrix
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 3rd 2019
 * ****************************************************************************/

#ifndef __C3_MATRIX_H__
#define __C3_MATRIX_H__

#include <stdio.h>

#define MAX_WORD_LEN 32

float **readMatrix(char *fileName, int *order);
void   writeMatrix(FILE *out, float **matrix, int order);

float *product(float **m, float *v, int order);

void freeMatrix(float ***matrix, int order);

#endif /* __C3_MATRIX_H__ */

