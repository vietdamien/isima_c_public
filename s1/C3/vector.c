/* *****************************************************************************
 * Project name: C3
 * File name   : vector
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 3rd 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>

#include "vector.h"

void writeVector(FILE *out, float *vector, int order) {
    int i;

    fprintf(out, "[ ");
    for (i = 0; i < order; ++i) { fprintf(out, "%.3f ", vector[i]); }
    fprintf(out, "]");
}

float *readVector(char *fileName, int *order) {
    float tmpOrder, *vector = NULL;
    FILE *file;
    int i;

    if ((file = fopen(fileName, "r")) == NULL) {
        perror("readVector: fopen.\n");
        fprintf(stderr, "\tfile name: %s\n", fileName);
        return NULL;
    }

    fscanf(file, "%f%*c", &tmpOrder);
    if ((*order = tmpOrder) != tmpOrder) {
        fprintf(stderr, "readVector: order must be an int!\n");
        fclose(file);
        return NULL;
    }

    if ((vector = malloc(*order * sizeof(float))) == NULL) {
        perror("readVector: malloc.\n");
        exit(errno);
    }

    for (i = 0; i < *order; ++i) { fscanf(file, "%f ", &vector[i]); }

    fclose(file);
    return vector;
}

float scalarProduct(float *v1, float *v2, int order) {
    float result = 0;
    int i;

    for (i = 0; i < order; ++i) { result += v1[i] * v2[i]; }

    return result;
} 

void freeVector(float **vector) {
    free(*vector);
    *vector = NULL;
}

