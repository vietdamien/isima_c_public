/* *****************************************************************************
 * Project name: C3
 * File name   : matrix
 * Author      : Damien Nguyen
 * Date        : Wednesday, October 3rd 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>

#include "matrix.h"
#include "vector.h"

float **readMatrix(char *fileName, int *order) {
    float tmpOrder, **matrix = NULL;
    FILE *file;
    int i, j;

    if ((file = fopen(fileName, "r")) == NULL) {
        perror("readMatrix: fopen.\n");
        fprintf(stderr, "\tfile name: %s\n", fileName);
        return NULL;
    }

    fscanf(file, "%f%*c", &tmpOrder);
    if ((*order = tmpOrder) != tmpOrder) {
        fprintf(stderr, "readMatrix: order must be an int!\n");
        fclose(file);
        return NULL;
    }
    
    if ((matrix = malloc(*order * sizeof(float *))) == NULL) {
        perror("readMatrix: general malloc.\n");
        exit(errno);
    }

    for (i = 0; i < *order; ++i) {
        if ((matrix[i] = malloc(*order * sizeof(float))) == NULL) {
            perror("readMatrix: cell malloc.\n");
            exit(errno);
        }
    }

    for (i = 0; i < *order; ++i) {
        for (j = 0; j < *order; ++j) {
            fscanf(file, "%f ", &matrix[i][j]);
        }
    }

    fclose(file);
    return matrix;
}

void writeMatrix(FILE *out, float **matrix, int order) {
    int i, j;

    fprintf(out, "[\n");
    for (i = 0; i < order; ++i) {
        for (j = 0; j < order; ++j) {
            fprintf(out, "%.3f ", matrix[i][j]);
        }
        fprintf(out, "\n");
    }
    fprintf(out, "]");
}


float *product(float **m, float *v, int order) {
    float *result;
    int i, j;

    if ((result = calloc(order, sizeof(float))) == NULL) {
        perror("product: calloc.\n");
        return NULL;
    }

    for (i = 0; i < order; ++i) {
        for (j = 0; j < order; ++j) {
            result[i] += m[i][j] * v[j];
        }
    }

    return result;
}

void freeMatrix(float ***matrix, int order) {
    int i;

    for (i = 0; i < order; ++i) {
        free((*matrix)[i]);
    }
    free(*matrix);

    *matrix = NULL;
}

