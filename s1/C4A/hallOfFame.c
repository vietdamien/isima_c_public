/* *****************************************************************************
 * Project name: Hall of fame
 * File name   : hallOfFame
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 08 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "hallOfFame.h"

void readStr(FILE *in, char *str, size_t len) {
    int readLen = 0;

    if (fgets(str, len, in) && str[(readLen = strlen(str)) - 1] == '\n') {
        str[readLen - 1] =  '\0';
    }
}

void readData(FILE *in, data_t *data) {
    char str[DEFAULT_STR_LEN]   = "";
    char name[DEFAULT_NAME_LEN] = "", alias[DEFAULT_ALIAS_LEN] = "";
    int tmp = 0;

    readStr(in, name, DEFAULT_NAME_LEN);
    readStr(in, alias, DEFAULT_ALIAS_LEN);
    readStr(in, str, DEFAULT_STR_LEN);

    if (!sscanf(str, "%d%*c", &tmp)) {
        fprintf(stderr, "readData: NaN\n");
        exit(EXIT_FAILURE);
    }

    strcpy(data->name, name);
    strcpy(data->alias, alias);
    data->score = tmp;
}

int arrayFromFilename(char *fileName, data_t array[]) {
    FILE *file = NULL;
    int   size = 0;

    if ((file = fopen(fileName, "r")) == NULL) {
        perror("arrayFromFilename: fopen.\n");
        exit(errno);
    }

    do {
        data_t data = create();
        readData(file, &data);
        if (strcmp("", data.name) && strcmp("", data.alias)) {
            array[size++] = data;
        } else {
            freeFields(&data);
        }
    } while (!feof(file) && size < MAX_SIZE);

    fclose(file);
    return size;
}

void displayData(FILE *out, data_t data) {
    fprintf(out, "%s : %s avec %d\n", data.name, data.alias, data.score);
}

void inputData(FILE *in, data_t *data) {
    readData(in, data);
}

void freeFields(data_t *data) {
    free(data->name);
    free(data->alias);
}

data_t create(void) {
    data_t data;

    data.name  = (char *) malloc(DEFAULT_NAME_LEN  * sizeof(char));
    data.alias = (char *) malloc(DEFAULT_ALIAS_LEN * sizeof(char));

    if (data.name == NULL || data.alias == NULL) {
        perror("create: malloc.\n");
        exit(errno);
    }

    data.score = DEFAULT_SCORE;

    return data;
}

