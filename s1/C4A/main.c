/* *****************************************************************************
 * Project name: Hall of fame
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Tuesday, October 08 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "teZZt.h"
#include "hallOfFame.h"

BEGIN_TEST_GROUP(array_struct)
/*
TEST(fgets) {
    char example[] = "scanf, c'est pas bien\n";
    LOG(example);
    char str1[25] = "";
    char str2[10] = "";

    FILE *file = fmemopen(example, sizeof(example) + 1, "r");
    // REQUIRE ( NULL != file);

    fgets(str1, 25, file);
    LOG(str1);
    fclose(file);

    REQUIRE(strlen(example) == strlen(str1));

    file = fmemopen(example, sizeof(example) + 1, "r");
    REQUIRE ( NULL != file);

    fgets(str2, 10, file);
    LOG(str2);

    REQUIRE( strlen(example) > strlen(str2));

    fgets(str2, 10, file);
    LOG(str2);
    fclose(file);
}

// pas de test mais un example simple de manipulation
TEST(A) {
    struct data test = create();

    strcpy(test.name, "2048");
    strcpy(test.alias, "loic");
    test.score = 16000;

    printf("%s ", test.name);
    printf("%s ", test.alias);
    printf("%d ", test.score);

    freeFields(&test);
}

TEST(B) {
    struct data test = create();

    strcpy(test.name, "pokemon GO");
    strcpy(test.alias, "loic");
    test.score = 498;

    displayData(stdout, test);

    // creation du flux de texte => buffer
    char buffer[1024] = "";
    FILE *file = fmemopen(buffer, 1024, "w");
    REQUIRE ( NULL != file);

    displayData(file, test);
    fclose(file);

    CHECK( 0 == strcmp(buffer, "pokemon GO : loic avec 498\n"));

    freeFields(&test);
}

TEST(C) {
    data_t test = create();

    strcpy(test.name, "overwatch");
    strcpy(test.alias, "loic");
    test.score = 2300;

    displayData(stdout, test);

    // creation du flux de texte => buffer
    char buffer[1024];
    FILE *file = fmemopen(buffer, 1024, "w");
    REQUIRE ( NULL != file);

    displayData(file, test);
    fclose(file);

    CHECK( 0 == strcmp(buffer, "overwatch : loic avec 2300\n"));

    freeFields(&test);
}

TEST(Input) {
    struct data test = create();

    char buffer[1024] = "";
    strcpy(buffer, "rien\ndutout\n10");
    FILE *file = fmemopen(buffer, 1024, "r");
    // REQUIRE ( NULL != file);

    inputData(file, &test);
    fclose(file);

    displayData(stdout, test);

    CHECK( 0 == strcmp(test.name, "rien")   );
    CHECK( 0 == strcmp(test.alias, "dutout"));
    CHECK(10 == test.score                  );

    freeFields(&test);
}
*/
TEST(readFile) {
    data_t array[MAX_SIZE];
    int i, size = 0;

    // test d'un fichier non existant
    //size = arrayFromFilename("inconnu.txt", array);
    CHECK( 0 == size );

    // test du fichier example
    size = arrayFromFilename("jeu1.txt", array);
/*
    size = 2;
    data_t d1 = create(), d2 = create();

    strcpy(d1.name, "2048");
    strcpy(d1.alias, "loic");
    d1.score = 64236;

    strcpy(d2.name, "Minecraft");
    strcpy(d2.alias, "kiux");
    d2.score = 12304883;

    array[0] = d1;
    array[1] = d2;
*/
    REQUIRE( 2 == size );
    CHECK  ( 0 == strcmp(array[0].name, "2048"));
    CHECK  ( 0 == strcmp(array[0].alias, "loic")); // :-)
    CHECK  ( 64236 == array[0].score );
    CHECK  ( 0 == strcmp(array[1].name, "Minecraft"));
    CHECK  ( 0 == strcmp(array[1].alias, "kiux"));
    CHECK  ( 12304883 == array[1].score );

    for (i = 0; i < size; ++i) {
        freeFields(&array[i]);
    }
}

END_TEST_GROUP(array_struct)

int main(void) {
    RUN_TEST_GROUP(array_struct);
    return EXIT_SUCCESS;
}

