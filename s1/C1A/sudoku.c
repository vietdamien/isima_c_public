/*
* sudoku.c
*
* Trame du TP de C intitulé sudoku.c
* Seule la fonction generer() du TP est véritablement codée.
* Avant de coder, il faudra choisir si les variables grille et remplissage sont locales ou globales
*
* Loic - 1N/0N/2016
*/

#include <stdio.h>

#define N 9

int initialiser(int tab[][N])
{
    int i, j;

    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            tab[i][j] = 0;
        }
    }

	return 0;
}

void afficher(int tab[][N])
{
    int i, j;
    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            printf("%4d", tab[i][j]);
        }
        printf("\n");
    }
}

int generer(int tab[][N]) 
{
	
	int i, j;

	for(j=0;j<N; ++j) 
	{
		for(i=0; i<N; ++i)
			tab[j][i] = (i + j*3 +j /3) %N +1 ; 
	}

	//for(i=0;i<N; ++i)
	//{
	//	for(j=0; j<N; ++j)
	//		printf("%d ", tab[i][j]);
	//	printf("\n");
	//}

	return 81;
}

int saisir(int i, int j) 
{

	return 0;
}

int verifierLigneColonne(int i, int sens)
{
	return 0;
}

int verifierRegion(int k, int l)
{
	return 0;
}

int verifierGrille() {
	return 0;
}

int main()
{
    int tab[N][N];
    printf("INIT:\n");
    initialiser(tab);
    afficher(tab);
    printf("GENERATED:\n");
    generer(tab);
    afficher(tab);
	return 0;
}
