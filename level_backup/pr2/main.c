/**
 * TP C #2
 * Author: Damien NGUYEN
 * Date: Monday, September 16th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#include "tp.h"

int main(/*int argc, char const *argv[]*/) {
    double f1, f2, n;

    printf("===== Simple operation =====\n");
    printf("Result: %f.\n", calculate(inputChar("sign: "), inputNumber("a: "), inputNumber("b: ")));
    printf("===== Halves =====\n");
    printAllHalves(inputNumber("n: "));
    printf("===== Equation =====\n");
    solve(inputNumber("a: "), inputNumber("b: "), inputNumber("c: "));
    printf("===== Sum =====\n");
    printf("Sum: %f.\n", sum(inputNumber("n: "), inputNumber("p: ")));
    printf("===== Factorial =====\n");
    printf("f1 = %f.\n", f1 = factorial(n = inputNumber("n: ")));
    printf("===== Factorial (recursive) =====\n");
    printf("f2 = %f.\n", f2 = recursiveFactorial(n));

    printf("f1 and f2 are %sequal.\n", f1 == f2 ? "" : "not ");

    return EXIT_SUCCESS;
}

