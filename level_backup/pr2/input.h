/**
 * TP C #2
 * Author: Damien NGUYEN
 * Date: Monday, September 16th 2019
 */

#ifndef INPUT_C
#define INPUT_C

double inputNumber(char const *msg);
char inputChar(char const *msg);
char *inputLine(char const *msg);

#endif /* TP_C */

