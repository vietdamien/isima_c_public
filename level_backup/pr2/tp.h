/**
 * TP C #2
 * Author: Damien NGUYEN
 * Date: Monday, September 16th 2019
 */

#ifndef TP_C
#define TP_C

double calculate(char sign, double a, double b);
void printAllHalves(int n);
void solve(double a, double b, double c);
double sum(int n, double power);
double factorial(int n);
double recursiveFactorial(int n);

#endif /* TP_C */

