/**
 * TP C #2
 * Author: Damien NGUYEN
 * Date: Monday, September 16th 2019
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MINUS '-'
#define OVER '/'
#define PLUS '+'
#define TIMES '*'

double validate(double n) { return fabs(n); }

double calculate(char sign, double a, double b) {
    double result;

    switch (sign) {
        case MINUS:
            result = a - b;
            break;
        case OVER:
            if (b == 0) {
                fprintf(stderr, "calculate: division by 0!\n");
                exit(EXIT_FAILURE);
            }
            result = a / b;
            break;
        case PLUS:
            result = a + b;
            break;
        case TIMES:
            result = a * b;
            break;
        default:
            fprintf(stderr, "Wrong operation!\n");
            exit(EXIT_FAILURE);
    }

    return result;
}

void printAllHalves(int n) {
    printf("Halves: ");
    while (abs(n) > 0) {
        printf("%d ", n = n / 2);
    }
    printf("\n");
}

void solve(double a, double b, double c) {
    double delta = pow(b, 2) - 4*a*c;

    if (a == 0) {
        fprintf(stderr, "Not a second degree equation.\n");
        exit(EXIT_FAILURE);
    }

    if (delta < 0) {
        fprintf(stderr, "No solution.\n");
    } else if (delta == 0) {
        printf("One solution: %f.\n", -b/2*a);
    } else {
        printf("Two solutions: %f and %f.\n", (-b-sqrt(delta))/2*a, (-b+sqrt(delta))/2*a);
    }
}

double sum(int n, double power) {
    double sum = 0;
    int i;

    n = validate(n);

    for (i = 1; i <= n; ++i) {
        sum += pow(i, power);
    }

    return sum;
} 

double factorial(int n) {
    double result = 1;
    int i;

    for (i = 1; i <= n; ++i) {
        result *= i;
    }

    return result;
}

double recursiveFactorial(int n) {
    return n == 0 ? 1 : n * recursiveFactorial(n - 1);
}

