/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tp.h"

int init(void) {
    srand(time(NULL));
    return rand() % 100;
}

void game(int number) {
    int count = 0, expectation, guess;
    printf("Predict the number of tries: ");
    scanf(INT_INPUT_PATTERN, &expectation);
    do {
        printf("Your guess: ");
        scanf(INT_INPUT_PATTERN, &guess);
        ++count;
        printf("%s!\n", guess < number ? "Too low" : (guess == number ? "Bravo" : "Too high"));
    } while (count < expectation && guess != number);
    printf("You %s!\n", count <= expectation ? "won" : "lost");
    printf("Number of guesses: %d.\n", count);
}

