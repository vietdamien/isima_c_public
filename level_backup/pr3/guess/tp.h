/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#ifndef TP_C
#define TP_C

#include <stdbool.h>

#define INT_INPUT_PATTERN "%d%*c"

int init(void);
void game(int number);

#endif /* TP_C */

