/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#ifndef TP_C
#define TP_C

#define INT_INPUT_PATTERN "%d%*c"

double iterativeFibonacci(int n);
double recursiveFibonacci(int n);
void displayPascalTriangle(int n);

#endif /* TP_C */

