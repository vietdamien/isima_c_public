/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "tp.h"

double iterativeFibonacci(int n) {
    double array[(n = abs(n)) + 1];
    int i;

    for (i = 0; i <= n; ++i) {
        if      (i == 0) { array[i] = 0; }
        else if (i == 1) { array[i] = 1; }
        else {
            array[i] = array[i - 1] + array[i - 2];
        }
    }

    printf("Array: ");
    for (i = 0; i <= n; ++i) { printf("%f ", array[i]); }
    printf("\n");

    return array[n];
}

double recursiveFibonacci(int n) {
    bool b0, b1;
    double result;
    if ((b0 = n == 0)) { result = 0; };
    if ((b1 = n == 1)) { result = 1; };
    return b0 || b1 ? result : recursiveFibonacci(n - 2) + recursiveFibonacci(n - 1);
}

void displayPascalTriangle(int n) {
    int i, j;
    double triangle[n][n];

    for (i = 0; i < n; ++i) {
        triangle[i][i] = triangle[i][0] = 1;
        for (j = 1; j < i; ++j) {
            triangle[i][j] = triangle[i-1][j] + triangle[i-1][j-1];
        }
    }

    for (i = 0; i < n; ++i) {
        for (j = 0; j <= i; ++j) {
            if (triangle[i][j]) {
                printf("%.f ", triangle[i][j]);
            }
        }
        printf("\n");
    }
}

