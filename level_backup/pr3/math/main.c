/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include "tp.h"

int main(void) {
    int n;

    printf("FiboRec(4) = %.f.\n", recursiveFibonacci(4));
    printf("FiboIte(4) = %.f.\n", iterativeFibonacci(4));
    
    n = 6;
    displayPascalTriangle(n);
    //if ((triangle = malloc(dimension * sizeof(double *)))) {
    //    fprintf(stderr, "main: triangle rows allocation.\n");
    //    exit(EXIT_FAILURE);
    //}

    //for (i = 0; i < dimension; ++i) {
    //    if (triangle[i] = malloc(dimension * sizeof(double))) {
    //        fprintf(stderr, "main: triangle columns allocation.\n");
    //        exit(EXIT_FAILURE);
    //    }
    //}

    return EXIT_SUCCESS;
}

