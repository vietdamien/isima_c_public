/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "input.h"
#include "tp.h"

int main(void) {
    char *firstName = inputLine("First name: "), *lastName, *fullName, *sentence;
    int i, p = inputNumber("Number: ");

    for (i = 0; i < p; ++i) {
        printf("Merci %s.\n", firstName);
    }
    printf("myStrlen(%s): %lu.\n", firstName, myStrlen(firstName));
    printf("strlen(%s): %lu.\n", firstName, strlen(firstName));
    
    lastName = inputLine("\nLast name: ");

    if ((fullName = malloc(MAX_STR_LEN * sizeof(char))) == NULL) {
        fprintf(stderr, "main: lastName memory allocation problem.\n");
        exit(EXIT_FAILURE);
    }

    printf("Last name: %s.\n", lastName);
    strcpy(fullName, strcat(firstName, lastName));
    printf("(strcpy) Full name: %s.\n\n", fullName);
    
    myStrcat(fullName, lastName);
    printf("(myStrcat) Full name + last name: %s.\n", fullName);
    
    myStrcpy(firstName, fullName);
    printf("(myStrcpy) Full name into first name: %s.\n\n", firstName);
    
    printAscii(32, 127);
    
    sentence = inputLine("\nSentence: ");
    sentence = myToUppercase(sentence);
    printf("%s\n", sentence);

    return EXIT_SUCCESS;
}

