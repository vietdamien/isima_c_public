/**
 * TP C #2
 * Author: Damien NGUYEN
 * Date: Monday, September 16th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "input.h"

#define DOUBLE_INPUT_PATTERN "%lf%*c"
#define INT_INPUT_PATTERN    "%d%*c"
#define CHAR_INPUT_PATTERN   "%c%*c"

#define STR_PATTERN "%s"

char *simpleInput(char *msg) {
    char *result = malloc(MAX_STR_LEN * sizeof(char));
    if (result == NULL) {
        fprintf(stderr, "simpleInput: memory allocation.\n");
        exit(EXIT_FAILURE);
    }

    printf(STR_PATTERN, msg);
    
    fgets(result, MAX_STR_LEN, stdin);
    if (result[strlen(result) - 1] == '\n') {
        result[strlen(result) - 1] = '\0';
    }

    return result;
}

double inputNumber(char *msg) {
    double input;
    char *wholeLine = simpleInput(msg);

    if (sscanf(wholeLine, DOUBLE_INPUT_PATTERN, &input) != 1) {
        fprintf(stderr, "inputNumber: NaN.\n");
        exit(EXIT_FAILURE);
    }

    free(wholeLine);
    return input;
}

char inputChar(char *msg) {
    char input;
    char *wholeLine = simpleInput(msg);

    if (sscanf(wholeLine, CHAR_INPUT_PATTERN, &input) != 1) {
        fprintf(stderr, "inputChar: not a character.\n");
        exit(EXIT_FAILURE);
    }

    free(wholeLine);
    return input;
}


char *inputLine(char *msg) {
    return simpleInput(msg);
}

