/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#ifndef TP_C
#define TP_C

#define INT_INPUT_PATTERN "%d%*c"

char *myToUppercase(char *str);
size_t myStrlen(char *str);

char *myStrcat(char *dst, char *src);
char *myStrcpy(char *dst, char *src);

void printAscii(int min, int max);

#endif /* TP_C */

