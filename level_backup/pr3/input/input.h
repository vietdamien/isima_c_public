/**
 * TP C #2
 * Author: Damien NGUYEN
 * Date: Monday, September 16th 2019
 */

#ifndef INPUT_C
#define INPUT_C

#define MAX_STR_LEN 64

double inputNumber(char *msg);
char inputChar(char *msg);
char *inputLine(char *msg);

#endif /* TP_C */

