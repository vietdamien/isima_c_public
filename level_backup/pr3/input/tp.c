/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "tp.h"

char *myToUppercase(char *str) {
    char *newStr = malloc(128 * sizeof(char)), *tmp;
    if (newStr == NULL) {
        fprintf(stderr, "myToUppercase: newStr memory allocation.\n");
        exit(EXIT_FAILURE);
    }
    tmp = newStr;
    for (; *str; str++, tmp++) {
        *tmp = *str >= 97 && *str <= 122 ? *str - 32 : *str;
    }
    return newStr;
}

size_t myStrlen(char *str) {
    char *s;
    for (s = str; *s; ++s);
    return labs(s - str);
}

char *myStrcat(char *dst, char *src) {
    char *ptr = dst + myStrlen(dst);

    while (*src) { *ptr++ = *src++; }
    *ptr = '\0';

    return dst;
}

char *myStrcpy(char *dst, char *src) {
    char *ptr = dst;

    while (*src) { *ptr++ = *src++; }
    *ptr = '\0';

    return dst;
}

void printAscii(int min, int max) {
    int i;
    printf("ASCII (%d to %d): ", min, max);
    for (i = min; i < max; ++i) {
        printf("%c ", i);
    }
    printf("\n");
}

