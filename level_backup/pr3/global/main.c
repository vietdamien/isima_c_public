/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include "tp.h"

int main(void) {
    int i;

    for (i = 0; i < ARRAY_SIZE; ++i) {
        array[i] = 100;
    }
    addIndex(isEven, 1);
    addIndex(isOdd, -1);
    display();

    printf("Max: %d.\n", max());
    printf("Min: %d.\n", min());
    printf("Average: %f.\n", average());

    return EXIT_SUCCESS;
}

