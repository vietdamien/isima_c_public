/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#ifndef TP_C
#define TP_C

#define ARRAY_SIZE 50

#include <stdbool.h>

int array[ARRAY_SIZE];

bool isGreater(int a, int b);
bool isLower(int a, int b);

bool isEven(int number);
bool isOdd(int number);

double average(void);

int get(bool (*function)(int, int));
int max(void);
int min(void);

void addIndex(bool (*function)(int), int multiplier);
void display(void);

#endif /* TP_C */

