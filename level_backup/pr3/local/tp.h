/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#ifndef TP_C
#define TP_C

#define ARRAY_SIZE 50

#include <stdbool.h>

bool isGreater(int a, int b);
bool isLower(int a, int b);

bool isEven(int number);
bool isOdd(int number);

double average(int array[]);

int get(int array[], bool (*function)(int, int));
int max(int array[]);
int min(int array[]);

void addIndex(int array[], bool (*function)(int), int multiplier);
void display(int array[]);

#endif /* TP_C */

