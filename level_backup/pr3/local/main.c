/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include "tp.h"

int main(void) {
    int i, array[ARRAY_SIZE];

    for (i = 0; i < ARRAY_SIZE; ++i) {
        array[i] = 100;
    }

    addIndex(array, isEven, 1);
    addIndex(array, isOdd, -1);
    display(array);

    printf("Max: %d.\n", max(array));
    printf("Min: %d.\n", min(array));
    printf("Average: %f.\n", average(array));

    return EXIT_SUCCESS;
}

