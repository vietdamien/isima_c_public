/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <stdio.h>
#include "tp.h"

bool isGreater(int a, int b) { return a > b; }
bool isLower(int a, int b)   { return isGreater(-a, -b); }

bool isEven(int number) { return number % 2 == 0; }
bool isOdd(int number)  { return !isEven(number); }

int max(int array[]) { return get(array, isGreater); }
int min(int array[]) { return get(array, isLower); }

double average(int array[]) {
    double size = ARRAY_SIZE, sum = 0;
    int i;

    for (i = 0; i < ARRAY_SIZE; ++i) {
        sum += array[i];
    }

    return sum / size;
}

int get(int array[], bool (*function)(int, int)) {
    int i, value = array[0];

    for (i = 1; i < ARRAY_SIZE; ++i) {
        if ((*function)(array[i], value)) {
            value = array[i];
        }
    }

    return value;
}

void addIndex(int array[], bool (*function)(int), int multiplier) {
    int i;

    for (i = 0; i < ARRAY_SIZE; ++i) {
        if ((*function)(i)) {
            array[i] += multiplier * i;
        }
    }
}

void display(int array[]) {
    int i;
    printf("Array: ");
    for (i = 0; i < ARRAY_SIZE; ++i) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

