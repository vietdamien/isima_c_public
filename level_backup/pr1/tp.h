/**
 * TP C #1
 * Author: Damien NGUYEN
 * Date: Friday, September 13th 2019
 */

#ifndef TP_H
#define TP_H

void listInts(int n);
void listSquaredInts(int n);
int squaredIntsSum(int n);
double inverseSquaredInts(int n);
void multiplicationTables(void);
void solve(void);

#endif /* TP_H */

