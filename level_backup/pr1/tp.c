/**
 * TP C #1
 * Author: Damien NGUYEN
 * Date: Friday, September 13th 2019
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int getPositiveInt(int n) { return n < 0 ? abs(n) : n; }

void listPoweredInts(int n, int p) {
    int i;

    n = getPositiveInt(n);

    for (i = 1; i <= n; ++i) {
        printf("%.f ", pow(i, p));
    }

    printf("\n");
}

void listInts(int n)        { listPoweredInts(n, 1); }
void listSquaredInts(int n) { listPoweredInts(n, 2); }

int squaredIntsSum(int n) {
    int i, result = 0;

    n = getPositiveInt(n);
    for (i = 1; i <= n; ++i) {
        result += pow(i, 2);
    }

    return result;
}

double inverseSquaredInts(int n) {
    int i;
    double result = 0;

    if (n == 0) {
        fprintf(stderr, "n == 0: no inverse!\n");
        exit(EXIT_FAILURE);
    }

    for (i = 1; i <= n; ++i) {
        result += 1. / pow(i, 2);
    }

    return result;
}

void multiplicationTables(void) {
    int i, j;
    printf("===== MULTIPLICATION TABLES =====\n");
    for (i = 1; i <= 12; ++i) {
        printf("----- %d -----\n", i);
        for (j = 1; j <= 10; ++j) {
            printf("%d * %d = %d\n", i, j, i*j);
        }
    }
    printf("\n");
}

void solve(void) {
    double a = -2., b = 1., c = 3., delta;

    if (a == 0) {
        if (b == 0) {
            if (c == 0) {
                printf("Il existe une infinité de solutions.\n");
            } else {
                printf("Il n'y a pas de solutions.\n");
            }
        } else {
            printf("La solution est %f.\n", -c/b);
        }
    } else {
        delta = pow(b, 2) - 4*a*c;
        if (delta < 0) {
            printf("Aucune solution.\n");
        } else if (delta == 0) {
            printf("Une unique solution : %f.\n", -b/2*a);
        } else {
            printf("2 solutions : %f et %f.\n", (-b-sqrt(delta))/2*a, (-b+sqrt(delta)/2*a));
        }
    }
}
