/**
 * TP C #1
 * Author: Damien NGUYEN
 * Date: Friday, September 13th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include "tp.h"

int main() {
    solve();
    listInts(3);
    listSquaredInts(3);
    printf("Squared ints sum (2): %d.\n", squaredIntsSum(2));
    printf("Inverse squared ints (4): %f.\n", inverseSquaredInts(4));
    multiplicationTables();

    return EXIT_SUCCESS;
}

