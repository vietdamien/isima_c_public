/**
 * TP C #5
 * Author: Damien NGUYEN
 * Date: Wednesday, September 18th 2019
 */

#ifndef INPUT_C
#define INPUT_C

#define MAX_STR_LEN 64

char inputChar(char *msg);
char *inputLine(char *msg);

double inputNumber(char *msg);

int inputInt(char *msg);

#endif /* TP_C */

