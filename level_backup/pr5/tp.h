/**
 * TP C #5
 * Author: Damien NGUYEN
 * Date: Wednesday, September 18th 2019
 */

#ifndef TP_C
#define TP_C

#include <stdbool.h>

#define CLEAR_CHARACTER "\e[1;1H\e[2J"
#define DICTIONARY_SIZE 10
#define DICTIONARY_PATH "mots6.txt"
#define MAX_NB_TRIES    10
#define MAX_WORD_LEN    128
#define WORD_LEN        6

bool isCorrect(char *word, char *guess);

char **loadWords(void);

int menu(void);
int randInt(int nbWords);

void buildGuess(char *word, char *guess, char *attempt);
void initGuess(char *guess);

#endif /* TP_C */

