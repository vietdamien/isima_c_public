/**
 * TP C #5
 * Author: Damien NGUYEN
 * Date: Wednesday, September 18th 2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "input.h"
#include "tp.h"

bool isCorrect(char *word, char *guess) {
    return strcmp(word, guess) == 0;
}

char **loadWords(void) {
    char **words, tmp[MAX_WORD_LEN];
    FILE *file;
    int i, j, nbWords;

    if ((file = fopen(DICTIONARY_PATH, "r")) == NULL) {
        fprintf(stderr, "loadWord: fopen.\n");
        exit(EXIT_FAILURE);
    }

    fscanf(file, "%d%*c", &nbWords);
    if ((words = malloc(DICTIONARY_SIZE * sizeof(char *))) == NULL) {
        fprintf(stderr, "loadWords: words malloc.\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < DICTIONARY_SIZE; ++i) {
        if ((words[i] = malloc((WORD_LEN + 1) * sizeof(char))) == NULL) {
            fprintf(stderr, "loadWords: word[%d] malloc.\n", i);
            exit(EXIT_FAILURE);
        }
        for (j = 0; j < randInt(nbWords) - 1; ++j) { fgets(tmp, MAX_WORD_LEN, file); }
        fgets(words[i], MAX_WORD_LEN, file);
        if (words[i][strlen(words[i]) - 1] == '\n') {
            words[i][strlen(words[i]) - 1] = '\0';
        }
    }

    fclose(file);
    return words;
}

int menu(void) {
    printf("===== MOTUS =====\n1-Jouer\n0-Quitter\n");
    return inputInt("Choix : ");
}

int randInt(int nbWords) {
    srand(time(NULL));
    return (rand() % nbWords) / DICTIONARY_SIZE;
}

void buildGuess(char *word, char *guess, char *attempt) {
    int i;
    for (i = 0; i < WORD_LEN; ++i) {
        if (word[i] == attempt[i]) {
            guess[i] = word[i];
        } else if (strchr(word, attempt[i]) != NULL) {
            printf("Misplaced letter: %c.\n", attempt[i]);
        }
    }
}

void initGuess(char *guess) {
    char *tmp = guess;
    int i;

    for (i = 0; i < WORD_LEN; ++i) {
        tmp[i] = '*';
    }
    tmp[WORD_LEN] = '\0';
}

