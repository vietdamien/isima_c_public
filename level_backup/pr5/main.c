/**
 * TP C #5
 * Author: Damien NGUYEN
 * Date: Wednesday, September 18th 2019
 */

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "input.h"
#include "tp.h"

int main(void) {
    bool wordFound = false;
    char guess[WORD_LEN + 1], *attempt, **words = loadWords();
    int i, choice, currentIndex = 0, nbTries = 0;

    for (i = 0; i < DICTIONARY_SIZE; ++i) {
        printf("%d - %s\n", i, words[i]);
    }
    
    while (currentIndex < DICTIONARY_SIZE && (choice = menu()) != 0) {
        initGuess(guess);
        do {
            attempt = inputLine("Votre réponse : ");
            buildGuess(words[currentIndex], guess, attempt);
            printf("Result: %s\n", guess);
            ++nbTries;
            free(attempt);
        } while (!(wordFound = isCorrect(words[currentIndex], guess)) && nbTries < MAX_NB_TRIES);
        ++currentIndex;
        if (wordFound) {
            nbTries = 0;
            printf("%sFélicitations, passage au mot suivant.\n", CLEAR_CHARACTER);
        }
    }

    return EXIT_SUCCESS;
}

