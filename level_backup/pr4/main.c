/**
 * TP C #3
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "input.h"
#include "tp.h"

int main(void) {
    bool wasFound;
    char *word = loadWord(), *guess = initGuess(word);
    char attempt;
    int nbMistakes = 0;

    do {
        attempt = toupper(inputChar("Guess (letter): "));
        if (!guessRebuilt(word, guess, attempt)) {
            displayCartoon(++nbMistakes);
        }
        printf("%s\n", guess);
    } while (!(wasFound = wordFound(word, guess)) && nbMistakes <= MAX_NB_TRIES);

    if (wasFound) {
        printf("You won! [%d mistake%s]\n", nbMistakes, nbMistakes > 1 ? "s" : "");
    } else {
        printf("You lost! The word was: %s.\n", word);
    }

    return EXIT_SUCCESS;
}

