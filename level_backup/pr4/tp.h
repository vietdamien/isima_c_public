/**
 * TP C #4
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#ifndef TP_C
#define TP_C

#include <stdbool.h>

#define DICTIONARY_PATH "mots.txt"
#define MAX_NB_TRIES    10
#define MAX_WORD_LEN    128

bool guessRebuilt(char *word, char *guess, char attempt);
bool wordFound(char *word, char *guess);

char *loadWord(void);
char *initGuess(char *word);

int randInt(int nbWords);

void displayCartoon(int nbTries);

#endif /* TP_C */

