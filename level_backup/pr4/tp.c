/**
 * TP C #4
 * Author: Damien NGUYEN
 * Date: Tuesday, September 17th 2019
 */

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "tp.h"

static char cartoon[MAX_NB_TRIES][70] = {
    "\n\n\n\n\n\n_______\n",
    "\n   |\n   |\n   |\n   |\n   |\n___|___\n",
    "\n   /-----\n   |\n   |\n   |\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |\n   |\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |    |\n   |   \n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /|\n___|___\n"
};

bool guessRebuilt(char *word, char *guess, char attempt) {
    bool wasRebuilt = false;
    int i, length = strlen(word);
    if (!isupper(attempt)) {
        fprintf(stderr, "guessRebuit: not an uppercase letter!\n");
        exit(EXIT_FAILURE);
    }
    for (i = 0; i < length; ++i) {
        if (word[i] == attempt) {
            guess[i]   = attempt;
            wasRebuilt = true;
        }
    }
    return wasRebuilt;
}

bool wordFound(char *word, char *guess) {
    return strcmp(word, guess) == 0;
}

char *loadWord(void) {
    char *word, tmp[MAX_WORD_LEN];
    FILE *file;
    int i, nbWords;

    if ((file = fopen(DICTIONARY_PATH, "r")) == NULL) {
        fprintf(stderr, "loadWord: fopen.\n");
        exit(EXIT_FAILURE);
    }

    fscanf(file, "%d%*c", &nbWords);
    for (i = 0; i < randInt(nbWords) - 1; ++i) { fgets(tmp, MAX_WORD_LEN, file); }
    if ((word = malloc(strlen(tmp) * sizeof(char))) == NULL) {
        fprintf(stderr, "loadWord: malloc.\n");
        exit(EXIT_FAILURE);
    }

    strcpy(word, tmp);
    if (word[strlen(word) - 1] == '\n') {
        word[strlen(word) - 1] = '\0';
    }

    fclose(file);
    return word;
}

char *initGuess(char *word) {
    char *guess, *tmp;
    int i, length = strlen(word);
    if ((guess = malloc(length * sizeof(char))) == NULL) {
        fprintf(stderr, "main: malloc #1.\n");
        exit(EXIT_FAILURE);
    }

    tmp = guess;
    for (i = 0; i < length; ++i) {
        tmp[i] = '*';
    }

    return guess;
}

int randInt(int nbWords) {
    srand(time(NULL));
    return rand() % nbWords;
}

void buildGuess(char *guess, char attempt, int position) {
    guess[position - 1] = attempt;
}

void displayCartoon(int nbMistakes) {
    printf("\n%s\n", cartoon[nbMistakes - 1]);
}

